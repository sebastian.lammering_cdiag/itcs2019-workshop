package de.cdiag.cfworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudfoundryworkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudfoundryworkshopApplication.class, args);
	}

}
